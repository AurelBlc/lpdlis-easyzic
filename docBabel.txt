//Commande exécuter pour l'installation de Babel
npm init
npm install -D babel-cli
npm install -D babel-preset-env
npm install babel-polyfill --save
npm install core-js --save
npm install babel-plugin-transform-remove-strict-mode
touch .babelrc

// Contenu de .babelrc
{
  "presets": [
    ["env", {
      "targets": {
        "ie": "11"
      }
    }]
  ],
  "plugins": ["transform-remove-strict-mode"]
}

// Contenu de package.json
// src : répertoire des fichiers sources
// lib : répertoire des fichiers transpilés
...
"scripts": {
  "build": "babel src -d lib",
},
...

//Commande à exécuter pour la transpilation
npm run build