/**
 * Classe du lecteur de musique
 * @author Ewen
 * @author Raphael
 */
class Player {

    /**
     * Constructeur du playeur
     * @param playlist la playlist
     */
    constructor(playlist) {
        this.audioElt = document.createElement("audio");
        this.tsDebutLecture = null;
        this.playlist = playlist;
    }

    /**
     * Joue l'audio audioElt
     */
    play() {
        this.tsDebutLecture = Date.now()/1000;
        this.audioElt.play();
    }

    /**
     * Met en pause la musique.
     *
     */

    pause() {
        this.audioElt.pause();
        this.saveTempsEcoute();
    }

    /**
     * Pause la Pré-écoute
     */
    pausePreEcoute() {
        this.audioElt.pause();
        this.audioElt.currentTime = 0;
    }

    /**
     * Joue l'audio suivant dans la playlist
     */
    playNext() {
        this.audioElt.src = this.playlist.getNextTrack().getUrlAudio();
        this.play();
    }

    /**
     * Lecture de la piste précédente
     *
     */
    playPrev() {
        this.audioElt.src = this.playlist.getPrevTrack().getUrlAudio();
        this.play();
    }

    /**
    * Rends aléatoire la lecture
    */
    randomOn(){
        this.playlist.setRandom(true);
    }

    /**
    * Remets la lecture normale de la playlist
    */
    randomOff(){
        this.playlist.setRandom(false);
    }

    /**
    * Répète la musique en boucle
    */
    loopOn(){
        this.playlist.setLoop(true);
    }

    /**
    * Arrête la boucle de répétition
    */
    loopOff(){
        this.playlist.setLoop(false);
    }

    /**
    * Enregistre le temps d'écoute dans la track actuelle
    */
    saveTempsEcoute(){
        this.playlist.getCurrentTrack().addDureeEcoutee((Date.now()/1000)-this.tsDebutLecture);
    }

    displayPlayer(){
        var currentInstance = this;

        //Playlist
        var playlistDiv = document.getElementById("entirePlaylist");

        //Player
        var entirePlayer = document.getElementById("entirePlayer");
        this.audioElt = document.createElement("audio");
        var playerControl = document.createElement("div");
        var disque = document.createElement("div");
        var shareLove = document.createElement("div");
        shareLove.classList.add('shareLove');
        var displayPlaylist = document.createElement("div");

        //Boutons
        var btnPrev = document.createElement("button");
        var btnPlayPause = document.createElement("button");
        var btnNext = document.createElement("button");
        var btnShare = document.createElement("button");
        var btnLike = document.createElement("button");

        //Images
        var imgPrev = document.createElement("img");
        imgPrev.setAttribute("class", "im-prev");
        imgPrev.setAttribute("alt", "bouton prev");
        imgPrev.setAttribute("src", "../images/step-backward-solid.svg");

        var imgPlayPause = document.createElement("img");
        imgPlayPause.setAttribute("class", "im-play");
        imgPlayPause.setAttribute("alt", "bouton play/pause");
        imgPlayPause.setAttribute("src", "../images/play-solid.svg");
        
        var imgNext = document.createElement("img");
        imgNext.setAttribute("class", "im-next");        
        imgNext.setAttribute("alt", "bouton next");
        imgNext.setAttribute("src", "../images/step-forward-solid.svg");

        var imgDisque = document.createElement("img");
        imgDisque.setAttribute("class", "im-disc");     
        imgDisque.setAttribute("alt", "bouton disque");
        imgDisque.setAttribute("src", "../images/compact-disc-solid.svg");

        var imgShare = document.createElement("img");        
        imgShare.setAttribute("class", "im-share");  
        imgShare.setAttribute("alt", "bouton partager");
        imgShare.setAttribute("src", "../images/share.svg");

        var imgLike = document.createElement("img");      
        imgLike.setAttribute("class", "im-like");  
        imgLike.setAttribute("alt", "bouton like");
        imgLike.setAttribute("src", "../images/heart.svg");

        //Audio
        this.audioElt.setAttribute('src', '../track/1.mp3');
        this.audioElt.setAttribute('class', 'audio');
        this.audioElt.setAttribute('autobuffer', 'true');
        this.audioElt.setAttribute('autoplay', 'true');

        playerControl.appendChild(btnPrev);
        playerControl.appendChild(btnPlayPause);
        playerControl.appendChild(btnNext);
        btnPrev.appendChild(imgPrev);
        btnPlayPause.appendChild(imgPlayPause);
        btnNext.appendChild(imgNext);
        disque.appendChild(imgDisque);
        shareLove.appendChild(btnShare);
        shareLove.appendChild(btnLike);
        btnShare.appendChild(imgShare);
        btnLike.appendChild(imgLike);

        var progressMusic = document.createElement("div");
        var start_time = document.createElement("small");
        start_time.setAttribute("id", "start-time");
        var seekObj = document.createElement("progress");
        seekObj.setAttribute("id", "seekObj");
        seekObj.setAttribute("value", "0");
        seekObj.setAttribute("max", "1");
        var end_time = document.createElement("small");
        end_time.setAttribute("id", "end-time");

        progressMusic.appendChild(start_time);
        progressMusic.appendChild(seekObj);
        progressMusic.appendChild(end_time);

        var listeningMode = document.createElement("div");
        listeningMode.classList.add('listeningMode');

        var btnRandom = document.createElement("button");
        btnRandom.setAttribute("title", "Shuffle");
        var imgRandom = document.createElement("img");
        imgRandom.setAttribute("src", "../images/random-solid.svg");
        imgRandom.setAttribute("alt", "Aléatoire");
        btnRandom.appendChild(imgRandom);
        listeningMode.appendChild(btnRandom);

        var btnLoop = document.createElement("button");
        btnLoop.setAttribute("title", "Repeat");
        var imgLoop = document.createElement("img");
        imgLoop.setAttribute("src", "../images/repeat.svg");
        imgLoop.setAttribute("alt", "Répéter");
        btnLoop.appendChild(imgLoop);
        listeningMode.appendChild(btnLoop);


        var volumeSpan = document.createElement("button");
        volumeSpan.setAttribute("class", "volume");
        var btnVolume = document.createElement("button");
        btnVolume.setAttribute("title", "Volume");
        btnVolume.setAttribute("id", "btnVolume");
        var imgVolume = document.createElement("img");
        imgVolume.setAttribute("src", "../images/volume-up-solid.svg");
        imgVolume.setAttribute("alt", "Volume");
        btnVolume.appendChild(imgVolume);
        var volumeSliderTotale = document.createElement("span");
        volumeSliderTotale.setAttribute("class", "volume_sliderTotale");
        var volumeslider = document.createElement("span");
        volumeslider.setAttribute("class", "volume_slider");
        var volumeRange = document.createElement("input");
        volumeRange.setAttribute("class", "volumeRange");
        volumeRange.setAttribute("type", "range");
        volumeRange.setAttribute("min", "0");
        volumeRange.setAttribute("max", "100");
        volumeRange.setAttribute("value", "0");
        volumeRange.setAttribute("step", "1");
        volumeRange.setAttribute("style", "background-size: 50% 100%;");
        volumeslider.appendChild(volumeRange);
        volumeSliderTotale.appendChild(volumeslider);
        btnVolume.appendChild(volumeSliderTotale);
        btnVolume.appendChild(imgVolume);
        volumeSpan.appendChild(btnVolume);
        listeningMode.appendChild(volumeSpan);

        var btnFileAttente = document.createElement("button");
        btnFileAttente.setAttribute("id", "btnFileAttente");
        btnFileAttente.setAttribute("title", "Playlist");
        var imgFileAttente = document.createElement("img");
        imgFileAttente.setAttribute("src", "../images/playlist-plus.svg");
        imgFileAttente.setAttribute("alt", "playlist");
        imgFileAttente.textContent = "File d'attente";
        btnFileAttente.appendChild(imgFileAttente);
        displayPlaylist.appendChild(btnFileAttente);

        entirePlayer.appendChild(this.audioElt);
        entirePlayer.appendChild(playerControl);
        entirePlayer.appendChild(disque);
        entirePlayer.appendChild(shareLove);
        entirePlayer.appendChild(progressMusic);
        entirePlayer.appendChild(listeningMode);
        entirePlayer.appendChild(displayPlaylist);

        //Assigner des évènements aux boutons
        btnPrev.addEventListener("click", function(){
            currentInstance.playPrev();
        });

        btnPlayPause.addEventListener("click", function(){
            if(currentInstance.audioElt.paused){
                currentInstance.play();
                imgPlayPause.classList.replace('im-play','im-pause');
            }
            else{
                currentInstance.pause();
                imgPlayPause.classList.replace('im-pause','im-play');
            }
        });

        currentInstance.audioElt.addEventListener("ended", function(){
            currentInstance.saveTempsEcoute();
        });

        btnNext.addEventListener("click", function(){
            currentInstance.playNext();
        });

        btnRandom.addEventListener("click", function(){
            if(currentInstance.playlist.getRandom()){
                currentInstance.playlist.setRandom(false);
            }
            else{
                currentInstance.playlist.setRandom(true);
            }
        });

        btnLoop.addEventListener("click", function(){
            if(currentInstance.playlist.getLoop()){
                currentInstance.playlist.setLoop(false);
            }
            else{
                currentInstance.playlist.setLoop(true);
            }
        });

        btnFileAttente.addEventListener("click", function(){
            if(playlistDiv.classList.contains("active_playlist")){
                playlistDiv.classList.replace('active_playlist', 'fileAttente');
            } else {
                playlistDiv.classList.replace('fileAttente', 'active_playlist');
                currentInstance.playlist.repaint();
            }
        });

        volumeSpan.addEventListener("click", function(){
            if(volumeSpan.classList.contains("active_volume")){
                volumeSpan.classList.replace('active_volume', 'volume');
            } else {
                volumeSpan.classList.replace('volume', 'active_volume');
            }
        });

        btnVolume.addEventListener("click", function(){
            if(volumeslider.classList.contains("active_volume")){
                volumeslider.classList.replace('active_volume', 'volume_slider');
            } else {
                volumeslider.classList.replace('volume_slider', 'active_volume');
            }
        });

        btnLike.addEventListener("click", function(){
            var track = currentInstance.playlist.getCurrentTrack();
            if(currentInstance.playlist.getCurrentTrack().getLike()){
                currentInstance.playlist.setLike(track.id, false);
                imgLike.classList.replace('im-heart-plain','im-heart');
            }
            else{
                currentInstance.playlist.setLike(track.id, true);
                imgLike.classList.replace('im-heart','im-heart-plain');
            }
        });

        volumeslider.addEventListener("mousemove", function(){
            currentInstance.audioElt.volume = this.value / 100;
        });


        this.audioElt.addEventListener('timeupdate', function(){
            currentInstance.initProgressBar();
        });

    }
    //Calcule la durée totale en minutes:secondes
    calculateTotalValue(length) {
        var minutes = Math.floor(length / 60);
        var seconds_int = length - minutes * 60;
        var seconds_str = seconds_int.toString();
        var seconds = seconds_str.substr(0, 2);
        var time = minutes + ':' + seconds;

        return time;
    }

    //Calcule la durée actuelle en minutes:secondes
    calculateCurrentValue(currentTime) {
        var current_minute = parseInt(currentTime / 60) % 60,
            current_seconds_long = currentTime % 60,
            current_seconds = current_seconds_long.toFixed(),
            current_time = (current_minute < 10 ? "0" + current_minute : current_minute) + ":" + (current_seconds < 10 ? "0" + current_seconds : current_seconds);

        return current_time;
    }

    //Met à jour la barre de progression ainsi que la valeur d'avancement, permet aussi de cliquer sur la barre
    initProgressBar() {
        var currentInstance = this;
        var length = this.audioElt.duration;
        var current_time = this.audioElt.currentTime;

        var seekObj = document.getElementById('seekObj');
        seekObj.value = (this.audioElt.currentTime / this.audioElt.duration);
        seekObj.addEventListener("click", function (evt) {
            var percent = evt.offsetX / this.offsetWidth;
            currentInstance.audioElt.currentTime = percent * currentInstance.audioElt.duration;
            seekObj.value = percent / 100;
        });

        var end_time = document.getElementById('end-time');
        end_time.innerHTML = this.calculateTotalValue(length);
        var start_time = document.getElementById('start-time');
        start_time.innerHTML = this.calculateCurrentValue(current_time);

    }

}

document.addEventListener("DOMContentLoaded", showPlayer);

function showPlayer(){

    var tracksList = new Array();

    for (var i = 2763; i <= 2765; i++) {
        tracksList.push(Track.create(i));
    }
    for (i = 2797; i <= 2805; i++) {
        tracksList.push(Track.create(i));
    }
    for (i = 2904; i < 2911; i++) {
        tracksList.push(Track.create(i));
    }

    var playlist = new Playlist('Ma playlist', tracksList, false , false);
    var player = new Player(playlist);

    player.displayPlayer();
}
