/**
 * Classe Playlist
 * @param name le nom de la playlist
 * @param tracks tableau contenant des objets Track
 * @param random true si le mode random est activé, false sinon
 * @param loop true si le mode loop est activé, false sinon
 */
class Playlist {
    constructor(name, tracks, random, loop) {
        this.name = name;
        this.tracks = tracks;
        this.unreadTracks = tracks;
        this.currentTrackIndex = 0;
        this.prevTrackIndex = 0;
        this.random = random;
        this.loop = loop;
    }

    /**
    * Retourne le Track courant
    */
    getCurrentTrack() {
        return this.tracks[this.currentTrackIndex];
    }

    /**
    * Retourne le Track suivant.
    * Si le mode random est activé, renvoie un Track aléatoire présent dans la liste unreadTracks.
    */
    getNextTrack() {
        var nextTrack;
        this.prevTrackIndex = this.currentTrackIndex;
        if (this.random) {
            var nextRandomTrackIndex = Math.floor((Math.random() * this.unreadTracks.length) + 1);
            nextTrack = this.unreadTracks[nextRandomTrackIndex];
            this.unreadTracks.splice(nextRandomTrackIndex, 1);
        }
        else {
            nextTrack = this.unreadTracks[this.currentTrackIndex + 1];
            this.unreadTracks.splice(this.currentTrackIndex + 1, 1);
        }
        return nextTrack;
    }

    /**
    * Retourne le Track précédent.
    */
    getPrevTrack() {
        return this.tracks[this.prevTrackIndex];
    }

    /**
    * Ajoute un Track au début de la playlist
    */
    addTrackFirst(track) {
        this.tracks.unshift(track);
        //saveFile(this.tracks);
        this.repaint();
    }

    /**
    * Ajoute un Track à la fin de la playlist
    */
    addTrackLast(track) {
        this.tracks.push(track);
        //saveFile(this.tracks);
        this.repaint();
    }

    /**
    * Ajoute un Track dans la playlist après l'index passé en paramètre
    */
    addTrackAfter(track, refIndex) {
        this.tracks.splice(refIndex, 0, track);
        //saveFile(this.tracks);
        this.repaint();
    }

    /**
    * Supprime un Track dans la playlist
    * @param idTrack identifiant du Track
    */
    removeTrack(idTrack) {
        var trackIndex = this.getTrackIndex(idTrack);
        this.tracks.splice(trackIndex, 1);
        //saveFile(this.tracks);
        this.repaint();
    }

    /**
    * Supprime tous les Tracks dans la playlist
    */
    removeAllTracks() {
        this.tracks = [];
        this.unreadTracks = [];
        this.currentTrackIndex = 0;
        this.prevTrackIndex = 0;
        //saveFile(this.tracks);
        this.repaint();
    }

    /**
    * Récupère l'index du Track dans la playlist
    * @param idTrack identifiant du Track
    */
    getTrackIndex(idTrack) {
        //var idTrackInt = parseInt(idTrack);
        return this.tracks.map((track) => track.id).indexOf(idTrack);
    }

    /**
     * Fonction qui retourne la durée totale de la playList
     * Retourne une chaîne de caractères en h:m:s
     */
    getDureeTotale() {
        var dureeTotale = 0;
        for(var i = 0; i < this.tracks.length; i++) {
            dureeTotale += this.tracks[i].getDuree();
        }
        return this.dateHMS(dureeTotale);
    }

    /**
     * Fonction qui retourne la durée totale de la playList
     * Retourne une chaîne de caractères en 0 h 00 min
     */
    getDureeTotaleBis() {
        var dureeTotale = "";
        var tabDuree = this.getDureeTotale().split(":");
        if (tabDuree[0] !== "00") {
            if (tabDuree[0][0] === "0") dureeTotale += tabDuree[0][1];
            else dureeTotale += tabDuree[0];
            dureeTotale += " h ";
        }
        dureeTotale += tabDuree[1] + " min";

        return dureeTotale;
    }

    /**
     * Fonction qui permet de convertir une durée au format h:m:s
     * @param time
     * retourne une chaîne de caractères
     */
    dateHMS(time) {
        var addZero = function(v) { return v<10 ? '0' + v : v; };
        //js fonctionne en milisecondes
        var d = new Date(time * 1000);
        var t = [];
        t.push(addZero(d.getHours()-1));
        t.push(addZero(d.getMinutes()));
        t.push(addZero(d.getSeconds()));
        return t.join(':');
    }

    repaint() {
        //on utilise une variable pour l'instance courante permettant d'utiliser ses méthodes dans les fonctions sans conflits avec le "this"
        var currentInstance = this;

        var playlist = document.getElementById("playlist");
        var playlistHeader = document.getElementById("playlistHeader");
        //on réinitialise la liste pour ne pas avoir les données qui s'affichent plusieurs fois
        playlist.innerHTML = "";
        playlistHeader.innerHTML = "";

        if(this.tracks == null) {
            var message = document.createElement("h6");
            message.innerHTML = "La playlist est actuellement vide";
            playlist.appendChild(message);
        }
        else {
            var playlistInfos = document.createElement("p");
            playlistInfos.innerText = "File d'attente ";
            var tracksLength = this.tracks.length;
            var tracksLengthInfo = document.createElement("span");
            tracksLengthInfo.setAttribute("class", "duration");
            tracksLengthInfo.innerText = "- " + tracksLength + " titre" + ((tracksLength > 1) ? "s" : "") + " - " + this.getDureeTotaleBis();
            playlistInfos.appendChild(tracksLengthInfo);
            playlistHeader.appendChild(playlistInfos);
            var savePlaylistButton = document.createElement("button");
            savePlaylistButton.setAttribute("value", "Sauvegarder la playlist");
            savePlaylistButton.innerText = "Sauvegarder la playlist";
            savePlaylistButton.addEventListener("click", () => savePlaylist(this));
            playlistHeader.appendChild(savePlaylistButton);

            var table = document.createElement("table");
            var tableBody = document.createElement("tbody");
            table.appendChild(tableBody);
            playlist.appendChild(table);

            for(var i = 0; i < this.tracks.length; i++) {
                var track = this.tracks[i];
                var row = document.createElement("tr");
                var pictureCell = document.createElement("td");
                var likeCell = document.createElement("td");
                var titleCell = document.createElement("td");
                var menuCell = document.createElement("td");
                var artistCell = document.createElement("td");
                var infoCell = document.createElement("td");
                var actionCell = document.createElement("td");

                row.setAttribute("id", track.getId());
                row.setAttribute("draggable", "true");
                row.setAttribute("class", "draggable");

                var picture = document.createElement("img");
                picture.setAttribute("src", track.getUrlVisuel());
                pictureCell.appendChild(picture);
                var likeButton = document.createElement("button");
                var likeIcon = document.createElement("i");
                if (track.isLiked()) likeButton.setAttribute("class", "likeIcon fas fa-heart");
                else likeButton.setAttribute("class", "likeIcon far fa-heart");
                likeButton.setAttribute("name", track.getId());
                likeButton.setAttribute("data-value", track.isLiked());
                likeButton.appendChild(likeIcon);
                likeCell.appendChild(likeButton);
                titleCell.innerText = track.getTitre();
                artistCell.innerText = track.getArtiste();
                infoCell.setAttribute("class", "duration");
                infoCell.innerText = this.dateHMS(track.getDuree());
                var dropdownBtn = document.createElement("i");
                dropdownBtn.setAttribute("class", "dropdown dropdown-button");
                var dropdownButton = document.createElement("button");
                // dropdownButton.setAttribute("id", "dropdown-button");
                dropdownButton.setAttribute("onclick", "dropdown.toggleDropdown(this)");
                var dropdownIcone = document.createElement("i");
                dropdownIcone.setAttribute("class", "fas fa-ellipsis-v");
                dropdownButton.appendChild(dropdownIcone);
                dropdownBtn.appendChild(dropdownButton);
                menuCell.appendChild(dropdownBtn);
                dropdown.setTrack(track);

                var actionCellButton = document.createElement("button");
                actionCellButton.setAttribute("name", track.getId());
                actionCellButton.setAttribute("class", "deleteIcon far fa-trash-alt");
                actionCell.appendChild(actionCellButton);
                row.appendChild(pictureCell);
                row.appendChild(likeCell);
                row.appendChild(titleCell);
                row.appendChild(menuCell);
                row.appendChild(artistCell);
                row.appendChild(infoCell);
                row.appendChild(actionCell);
                tableBody.appendChild(row);
            }

            var likeButtons = document.getElementsByClassName("likeIcon");
            for(i = 0; i < likeButtons.length; i++) {
                likeButtons[i].addEventListener("click", function (e) {
                    var liked = (e.target.getAttribute("data-value") === "true");
                    currentInstance.setLike(e.target.name, !liked);
                });
            }

            var deleteButtons = document.getElementsByClassName("deleteIcon");
            for(i = 0; i < deleteButtons.length; i++) {
                deleteButtons[i].addEventListener("click", function (e){
                    currentInstance.removeTrack(e.target.name);
                });
            }
            var draggableItems = document.getElementsByClassName("draggable");
            for(i = 0; i < draggableItems.length; i++) {
                draggableItems[i].addEventListener("dragstart", function (e) {
                    currentInstance.onDragStart(e);
                });
                draggableItems[i].addEventListener("dragover", function (e) {
                    currentInstance.onDragOver(e);
                });
                draggableItems[i].addEventListener("drop", function (e) {
                    currentInstance.onDrop(e);
                });
            }
        }
    }

    /**
    * Gère le départ du drag
    * @param e évènement
    */
    onDragStart(e) {
        var idRow = e.target.id;
        //On ajoute les données transportées
        e.dataTransfer.setData("text", idRow);
        //On définit l'effet de déplacement
        e.dataTransfer.dropEffect = "copy";
    }

    /**
    * Gère le survol du drag
    * @param e évènement
    */
    onDragOver(e) {
        e.preventDefault();
        e.dataTransfer.dropEffect = "move";
    }

    /**
    * Gère la réception du drop
    * @param e évènement
    */
    onDrop(e) {
        e.preventDefault();
        //On récupère l'identifiant de la source
        var idSource = e.dataTransfer.getData("text");
        var indexTrackSource = this.getTrackIndex(idSource);
        //On récupère l'identifiant de la cible
        var idCible = e.target.closest('tr').id;
        var indexTrackCible = this.getTrackIndex(idCible);
        //On place la source après la cible
        this.moveTrackAfter(indexTrackSource, indexTrackCible);
    }

    /**
    * Déplace le Track après la position voulue
    * @param tIndex l'index du track que l'on veut déplacer
    * @param refIndex l'index vers lequel on veut déplacer le track
    */
    moveTrackAfter(tIndex, refIndex) {
        var track = this.tracks[tIndex];
        this.tracks.splice(tIndex, 1);
        this.tracks.splice(refIndex, 0, track);
        this.repaint();
    }

    /**
     * Getter de l'attribut name
     */
    getName() {
        return this.name;
    }

    /**
     * Setter de l'attribut name.
     * @param name le nom de la playlist
     */
    setName(name) {
        this.name = name;
    }

    /**
    * Getter de l'attribut tracks
    */
    getTracks() {
        return this.tracks;
    }

    /**
    * Setter de l'attribut tracks
    */
    setTracks(tracks) {
        this.tracks = tracks;
    }

    /**
    * Getter de l'attribut unreadTracks
    */
    getUnreadTracks() {
        return this.unreadTracks;
    }

    /**
    * Setter de l'attribut unreadTracks
    */
    setUnreadTracks(unreadTracks) {
        this.unreadTracks = unreadTracks;
    }

    /**
    * Getter de l'attribut currentTrackIndex
    */
    getCurrentTrackIndex() {
        return this.currentTrackIndex;
    }

    /**
    * Setter de l'attribut currentTrackIndex
    */
    setCurrentTrackIndex(currentTrackIndex) {
        this.currentTrackIndex = currentTrackIndex;
    }

    /**
     * Getter de l'attribut prevTrackIndex.
     */
    getPrevTrackIndex() {
        return this.prevTrackIndex;
    }

    /**
     * Setter de l'attribut prevTrackIndex.
     */
    setPrevTrackIndex(prevTrackIndex) {
        this.prevTrackIndex = prevTrackIndex;
    }

    /**
    * Getter de l'attribut random
    */
    getRandom() {
        return this.random;
    }

    /**
    * Setter de l'attribut random
    */
    setRandom(random) {
        this.random = random;
    }

    /**
    * Getter de l'attribut loop
    */
    getLoop() {
        return this.loop;
    }

    /**
    * Setter de l'attribut loop
    */
    setLoop(loop) {
        this.loop = loop;
    }

    /**
     * Modifie l'attribut like du Track
    * @param idTrack identifiant du Track
    * @param isLiked booléen
     */
    setLike(idTrack, isLiked) {
        //var idTrackInt = parseInt(idTrack);
        this.tracks.forEach(function(track) {
            if (track.id === idTrack) track.setLike(isLiked);
        });
        this.repaint();
    }
}

//Tests*********************************************************************************************************************

/* var tracksList = new Array();

for (var i = 1; i <= 10; i++) {
    tracksList.push(Track.create(i));
}

function test() {
    var playlistTest = new Playlist("Ma playlist", tracksList, false, false);
    playlistTest.repaint();
} */
