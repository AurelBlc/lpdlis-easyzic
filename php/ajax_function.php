<?php
require 'function.php';
session_start();

const FONCTION = 'function';
const ID_FICHIER = 'id_fichier';

//Recupere le request payload
$requestPayload = file_get_contents("php://input");
//Si il existe c'est qu'une methode post à été appelée
$object = json_decode($requestPayload,true);
//L'object contient le nom de la fonction de l'api à appeler
//on indique donc quelle fonction utilisée, et on utilise les reste des informations contenus dans l'objet
if(isset($object)){
    if (isset($object[FONCTION])) {
        if($object[FONCTION] == "like"){
            like(session_id(),$object[ID_FICHIER]);
        }
        elseif($object[FONCTION] == "unlike"){
            unlike(session_id(),$object[ID_FICHIER]);
        }
        elseif($object[FONCTION] == "setReadTime"){
            setReadTime(session_id(),$object[ID_FICHIER],$object['sec']);
        }
    }
}

else{
    //à l'aide du parametre f on récupère la fonction de l'API qui doit être appelée
        if(isset($_GET["f"])){
            if($_GET["f"]=="trackjson"){
                //On doit récupérer les infos de la musique en fonction de l'id de celle ci
                //On vérifie donc qu'elle est existante et on appelle la fonction de l'api
                //le 2eme parametre est l'id session de l'utilisateur
                if(isset($_GET["id"])){
                    echo trackjson($_GET["id"],session_id());
                }
            }
            elseif($_GET["f"]=="getSample"){
                echo getSample();
                    
            }
            elseif($_GET["f"]=="getListArtist"){
                echo getListArtist();
                    
            }
        }
            
    }

?>