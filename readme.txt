################ Projet professionnel Alternants ################


# Installation
- Version PHP >= 7.0.x
- Importer le dump du fichier dlis_player_bdd.sql dans une base de données mysql (>= 5.7.x) sur le même serveur (localhost)
avec un accès (user: 'admin', pass: 'admin')


# Point d'entrée
- Le point d'entrée de l'application est "[localhost]/easyzic/php/template.php"
- Musique du préplayer et du player stockée dans ./track/1.mp3 (Boulevard des airs, Le Bagad de Lann-Bihoué)
