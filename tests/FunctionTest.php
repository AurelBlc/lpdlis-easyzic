<?php
require '../../php/function.php';
use PHPUnit\Framework\TestCase;

/**
* Classe qui permet de tester les différentes fonctions du fichier function.php
*/
class FunctionTest extends TestCase
{
  /**
  * Test pour la fonction getListByArtist()
  */
  public function testGetListByArtist(){

    $array = getListByArtist(2129);

    for($i=0;$i<count($array);$i++){
        //Les musiques correspondent au bon artiste
        $this->assertEquals(2129,$array[$i]['id_profil_artiste']);
        //Tous les champs de la table sont récuperé
        $this->assertCount(16,$array[$i]);
    }
  }

  /**
  * Test pour la fonction getSample()
  */
  public function testGetSample(){

    $array = getSample();
    //Le tableau contient 7 morceaux
    $this->assertCount(7,$array);
    //Tous les champs de la table sont récuperé
    $this->assertCount(16,$array[0]);
  }
}
