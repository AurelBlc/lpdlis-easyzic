describe("PLAYLIST", function(){
    var playlist;
    var nom = "Ma playlist";
    var props = [
        {
           id: 1,
           titre: "House of The Rising Sun",
           artiste: "The Animals",
           duree: "00:08:53.00",
           urlAudio: "",
           urlVisuel: "",
           urlPartage: "",
           dureeEcoute: 1,
           read: 1,
           liked: false,
           validUntil: ""
       },
       {
          id: 2,
          titre: "Sultans of Swing",
          artiste: "Dire Straits",
          duree: "00:05:45.00",
          urlAudio: "",
          urlVisuel: "",
          urlPartage: "",
          dureeEcoute: 1,
          read: 1,
          liked: false,
          validUntil: ""
      }
    ];

    var track1;
    var track2;
    var trackList;

    beforeEach(function(){
        //on mock une div html avec comme id "playlist" pour la méthode repaint
        var dummyElement = document.createElement("div");
        dummyElement.setAttribute("id", "playlist");
        document.getElementById = jasmine.createSpy('HTML Element').and.returnValue(dummyElement);

        //on créé les tracks
        ApiConnector.getInstance = jasmine.createSpy().and.returnValue(ApiConnector);
        ApiConnector.getTrackInfos = jasmine.createSpy().and.returnValue(props[0]);
        track1 = new Track(props[0]);
        ApiConnector.getTrackInfos = jasmine.createSpy().and.returnValue(props[1]);
        track2 = new Track(props[1]);

        //on créé la playlist
        trackList = [track1, track2];
        playlist = new Playlist("Ma playlist", trackList, false, false);
    });

    it("#getCurrentTrack - retourne le track courant", function() {
        var track = playlist.getCurrentTrack();
        expect(track).toBe(trackList[0]);
    });

    it("#getNextTrack - retoune le track suivant", function() {
        expect(playlist.getNextTrack()).toBe(track2);
    });

    it("#getPrevTrack - retourne le track précédent", function() {
        playlist.setCurrentTrackIndex(1);
        var prevTrack = playlist.getPrevTrack();
        expect(prevTrack).toBe(trackList[0]);
    });

    it("#getTrackIndex - retourne l'index du track dans la playlist", function() {
        var index = playlist.getTrackIndex(track1.getId());
        expect(index).toEqual(playlist.getTracks().indexOf(track1));
    });

    it("#repaint - ", function() {
        //testé avec Playlist.html
    });

    it("#moveTrackAfter - déplace le track à une autre position", function() {
        playlist.moveTrackAfter(0,1);
        expect(playlist.getTracks().indexOf(track1)).toEqual(1);
    });

    it("#dateHMS - convertie la durée au format m:s:ms", function() {
        var timeFormat = "08:53:35";
        expect(playlist.dateHMS(32015)).toEqual(timeFormat);
    });

    it("#getDureeTotale - retourne la durée totale de la playlist", function() {
        var totalTime = track1.getDuree() + track2.getDuree();
        expect(playlist.getDureeTotale()).toEqual(playlist.dateHMS(totalTime));
    });

    it("#setLike - modifie l'attribut like du Track", function() {
        var isLiked = true;
        Track.setLike = jasmine.createSpy().and.returnValue(isLiked);
        ApiConnector.getInstance = jasmine.createSpy().and.returnValue(ApiConnector);
        ApiConnector.like = jasmine.createSpy().and.returnValue(true);
        ApiConnector.unlike = jasmine.createSpy().and.returnValue(false);
        playlist.setLike(track1.getId(), isLiked);
        expect(track1.isLiked()).toBeTruthy();
    });

    describe("Ajout et suppression de Tracks dans la playlist", function() {
        beforeEach(function() {
            trackList = [];
            trackList[0] = track1;
            playlist = new Playlist(nom, trackList, false, false);
        });

        it("#addTrackFirst - ajoute un track en début de la playlist", function() {
            playlist.addTrackFirst(track2);
            var tracks = playlist.getTracks();
            expect(tracks[0]).toEqual(track2);
        });

        it("#addTrackLast - ajoute un track à la fin de la playlist", function() {
            playlist.addTrackLast(track2);
            var tracks = playlist.getTracks();
            expect(tracks[1]).toEqual(track2);
        });

        it("#addTrackAfter - ajoute un track après l'indice passé en argument", function() {
            playlist.addTrackAfter(track2, 1);
            var tracks = playlist.getTracks();
            expect(tracks[1]).toEqual(track2);
        });

        it("#removeTrack - supprime un track de la playlist", function() {
            playlist.removeTrack(track1.getId());
            expect(playlist.getTracks().length).toEqual(0);
        });

        it("#removeAllTracks - supprime tous les tracks de la playlist", function() {
            playlist.removeAllTracks();
            expect(playlist.getTracks().length).toBe(0);
            expect(playlist.getUnreadTracks().length).toBe(0);
            expect(playlist.getCurrentTrackIndex()).toBe(0);
            expect(playlist.getPrevTrackIndex()).toBe(0);
        });
    });

    describe("Getter et setter", function() {

        it("#getName", function() {
            expect(playlist.getName()).toBe(nom);
        });

        it("#setName", function() {
            playlist.setName("new name");
            expect(playlist.getName()).toEqual("new name");
        });

        it("#getTracks", function() {
            expect(playlist.getTracks()).toBe(trackList);
        });

        it("#setTracks", function() {
            var newTrackList = [];
            playlist.setTracks(newTrackList);
            expect(playlist.getTracks()).toBe(newTrackList);
        });

        it("#getUnreadtracks", function() {
            expect(playlist.getUnreadTracks()).toEqual(trackList);
        });

        it("#setUnreadtracks", function() {
            var newUnreadTrack = null;
            playlist.setUnreadTracks(newUnreadTrack);
            expect(playlist.getUnreadTracks()).toBeNull();
        });

        it("#getCurrentTrackIndex", function() {
            expect(playlist.getCurrentTrackIndex()).toEqual(0);
        });

        it("#setCurrentTrackIndex", function() {
            var newIndex = 1;
            playlist.setCurrentTrackIndex(newIndex);
            expect(playlist.getCurrentTrackIndex()).toEqual(newIndex);
        });

        it("#getPrevTrackIndex", function() {
            expect(playlist.getPrevTrackIndex()).toEqual(0);
        });

        it("#setPrevTrackIndex", function() {
            var newIndex = 1;
            playlist.setPrevTrackIndex(newIndex);
            expect(playlist.getPrevTrackIndex()).toEqual(newIndex);
        });

        it("#getRandom", function() {
            expect(playlist.getRandom()).toBeFalsy();
        });

        it("#setRandom", function() {
            playlist.setRandom(true);
            expect(playlist.getRandom()).toBeTruthy();
        });

        it("#getLoop", function() {
            expect(playlist.getLoop()).toBeFalsy();
        });

        it("#setLoop", function() {
            playlist.setLoop(true);
            expect(playlist.getLoop()).toBeTruthy();
        });
    });
});