describe("TRACK", function() {

    var track1;
    var props = [
        {
            id: 1,
            titre: "House of The Rising Sun",
            artiste: "The Animals",
            duree: "00:08:53.00",
            urlAudio: "urlAudio",
            urlVisuel: "urlVisuel",
            urlPartage: "urlPartage",
            dureeEcoute: "1",
            read: 1,
            liked: 0,
            validUntil: ""
       }
    ];
    beforeEach(function() {
        ApiConnector.getInstance = jasmine.createSpy().and.returnValue(ApiConnector);
        ApiConnector.getTrackInfos = jasmine.createSpy().and.returnValue(props[0]);
        track1 = new Track(1);
    });

   it("#hydrateFromApi - récupère les infos du track", function() {
        var trackTest = new Track(1);
        expect(trackTest).toEqual(track1);
    });

    it("#dureeFromStringToFloat", function() {
        expect(track1.dureeFromStringToFloat("00:05:45.00")).toEqual(345);
    });

    describe("Getter et setter", function() {
        it("#getId", function() {
            expect(track1.getId()).toEqual(1);
        });

        it("#getArtiste", function() {
            expect(track1.getArtiste()).toEqual("The Animals");
        });

       /* it("#getDuree", function() {
            expect(track1.getDuree()).toEqual(533);
        });*/

        it("#getUrlAudio", function() {
            expect(track1.getUrlAudio()).toEqual("urlAudio");
        });

        it("#getUrlVisuel", function() {
            expect(track1.getUrlVisuel()).toEqual("urlVisuel");
        });

        /*it("#getDureeEcoute", function() {
            expect(track1.getDureeEcoute()).toEqual(1);
        });*/

        it("#isLiked", function() {
            expect(track1.isLiked()).toBeFalsy();
        });

        it("#setLike", function() {
            ApiConnector.getInstance = jasmine.createSpy().and.returnValue(ApiConnector);
            ApiConnector.like = jasmine.createSpy().and.returnValue(true);
            ApiConnector.unlike = jasmine.createSpy().and.returnValue(false);
            track1.setLike(true);
            expect(track1.isLiked()).toBeTruthy();
        });

        it("#addDuree", function() {
            track1.addDureeEcoutee(10);
            expect(track1.getDureeEcoute()).toEqual(11);
        });
    });

});